import 'package:envision_ocr_assignment/features/library/view/library.dart';
import 'package:flutter/material.dart';

class Library extends StatefulWidget {
  Library({Key? key}) : super(key: key);

  @override
  LibraryController createState() => LibraryController();
}

class LibraryController extends State<Library> {
  @override
  Widget build(BuildContext context) => LibraryView(this);
}