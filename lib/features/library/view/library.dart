import 'package:envision_ocr_assignment/core/widget_view.dart';
import 'package:envision_ocr_assignment/features/library/domain/controller/library_controller.dart';
import 'package:envision_ocr_assignment/features/library/state/library_cubit.dart';
import 'package:envision_ocr_assignment/features/library/state/library_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LibraryView extends StatefulWidgetView<Library, LibraryController> {
  const LibraryView(LibraryController controller) : super(controller);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LibraryCubit, LibraryState>(builder: (context, state) {
      if (state is LoadedState && state.paragraphs.isNotEmpty) {
        return ListView.separated(
          itemCount: state.paragraphs.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(state.paragraphs.keys.elementAt(index)),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        );
      } else {
        return Container(
          child: Center(
            child: Icon(
              Icons.block_flipped,
              size: 120,
              color: Colors.grey.withOpacity(0.6),
            ),
          ),
        );
      }
    });
  }
}