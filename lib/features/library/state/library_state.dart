import 'package:equatable/equatable.dart';

abstract class LibraryState extends Equatable {}

class InitialState extends LibraryState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LibraryState {
  @override
  List<Object> get props => [];
}

class LoadedState extends LibraryState {
  LoadedState(this.paragraphs);

  final Map<String, dynamic> paragraphs;

  @override
  List<Object> get props => [paragraphs];
}

class SavingState extends LibraryState {
  @override
  List<Object> get props => [];
}

class ErrorState extends LibraryState {

  ErrorState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
