
import 'package:envision_ocr_assignment/core/constants.dart';
import 'package:envision_ocr_assignment/core/shared_preferences_helper.dart';
import 'package:envision_ocr_assignment/features/library/state/library_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class LibraryCubit extends Cubit<LibraryState> {

  LibraryCubit(this.prefs) : super(InitialState());
  final SharedPreferencesHelper prefs;

  Future<void> retrieve() async {
    try {
      emit(LoadingState());
      Map<String, dynamic> prefsMap = await _getItems();

      emit(LoadedState(prefsMap));
    } catch(e){
      emit(ErrorState(e.toString()));
    }
  }

  Future<Map<String, dynamic>> _getItems() async {
    final keys = await prefs.getKeys();

    final prefsMap = Map<String, dynamic>();
    for(String key in keys) {
      prefsMap[key] = prefs.getData(key);
    }
    return prefsMap;
  }

  Future<void> saveItem(String paragraph) async {
    try {
      emit(SavingState());
      String dateKey = keyFormat.format(DateTime.now());
      await prefs.setData(dateKey, paragraph);
      Map<String, dynamic> prefsMap = await _getItems();
      emit(LoadedState(prefsMap));
    } catch(e){
      emit(ErrorState(e.toString()));
    }
  }
}