import 'dart:io';

import 'package:envision_ocr_assignment/features/capture/domain/extensions.dart';
import 'package:equatable/equatable.dart';


abstract class CapturesState extends Equatable {}

class InitialState extends CapturesState {
  @override
  List<Object> get props => [];
}

class OpenCameraState extends CapturesState {
  @override
  List<Object> get props => [];
}

abstract class ParagraphState extends CapturesState {
  ParagraphState(this.paragraphs);
  final List<String> paragraphs;
  bool get isValid;
  @override
  List<Object> get props => [];
}

class SaveToLibraryState extends CapturesState implements ParagraphState {
  SaveToLibraryState(this.paragraphs);
  final List<String> paragraphs;
  bool get isValid => true;
  @override
  List<Object> get props => [paragraphs, isValid];
}

abstract class FileReadyState extends CapturesState {
  final File image;
  FileReadyState(this.image);
  @override
  List<Object> get props => [image];
}

class LoadingState extends FileReadyState {
  final File image;
  LoadingState(this.image) : super(image);
  @override
  List<Object> get props => [image];
}

class LoadedState extends FileReadyState implements ParagraphState{
  LoadedState(this.file, this.eitherParagraphs) : super(file);

  final EitherCapture eitherParagraphs;
  final File file;

  @override
  List<Object> get props => [eitherParagraphs, file];

  @override
  List<String> get paragraphs => eitherParagraphs.paragraphList;

  @override
  bool get isValid => eitherParagraphs.isRight();
}

class ErrorState extends CapturesState {

  ErrorState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}