import 'dart:io';

import 'package:envision_ocr_assignment/features/capture/domain/i_ocr_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'capture_state.dart';

@injectable
class CaptureCubit extends Cubit<CapturesState> {
  final IOcrService ocrService;
  CaptureCubit({required this.ocrService}) : super(InitialState());

  void reset() => emit(InitialState());

  void openCamera() => emit(OpenCameraState());

  void saveToLibrary(List<String> paragraphs) => emit(SaveToLibraryState(paragraphs));

  Future<void> processImage(File image) async {
    try {
      emit(LoadingState(image));
      final paragraphs = await ocrService.process(image.path);
      emit(LoadedState(image, paragraphs));
    } catch (e) {
      emit(ErrorState(e.toString()));
    }
  }
}