import 'package:envision_ocr_assignment/features/capture/application/capture_model_impl.dart';
import 'package:envision_ocr_assignment/features/capture/application/paragraph_impl.dart';
import 'package:envision_ocr_assignment/features/capture/domain/i_capture_model.dart';

class OcrResponseDao {
  late Response response;

  OcrResponseDao({required this.response});

  OcrResponseDao.fromJson(Map<String, dynamic> json) {
    if (json['response'] != null) {
      response = Response.fromJson(json['response']);
    } else {
      response = Response.empty();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response.toJson();
    return data;
  }
}

class Response {
  late List<Paragraphs> paragraphs;

  Response({required this.paragraphs});
  Response.empty() : paragraphs = List<Paragraphs>.empty();

  Response.fromJson(Map<String, dynamic> json) {
    if (json['paragraphs'] != null) {
      paragraphs = json['paragraphs'].map<Paragraphs>((e) => Paragraphs.fromJson(e)).toList();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paragraphs'] = this.paragraphs.map((v) => v.toJson()).toList();
    return data;
  }
}

class Paragraphs {
  late String paragraph;
  late String language;

  Paragraphs({required this.paragraph, required this.language});

  Paragraphs.fromJson(Map<String, dynamic> json) {
    paragraph = json['paragraph'] ?? '';
    language = json['language'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paragraph'] = this.paragraph;
    data['language'] = this.language;
    return data;
  }
}

extension OcrResponseDaoExt on OcrResponseDao {
  List<ParagraphImpl> get paragraphs => response.paragraphs.map((e) => ParagraphImpl(content: e.paragraph, language: e.language)).toList();
  ICaptureModel get model => CaptureModelImpl(paragraphs: paragraphs);
}
