import 'package:envision_ocr_assignment/features/capture/state/capture_cubit.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_state.dart';
import 'package:envision_ocr_assignment/features/library/state/library_cubit.dart';
import 'package:flutter/material.dart';
import 'package:envision_ocr_assignment/core/widget_view.dart';
import 'package:envision_ocr_assignment/features/capture/domain/controller/ocr_result_controller.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OcrResultView extends StatelessWidgetView<OcrResult> {
  const OcrResultView(OcrResult controller) : super(controller);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<CaptureCubit>().reset();
        return false;
      },
      child: Scaffold(
        body: BlocConsumer<CaptureCubit, CapturesState>(listener: (context, state) {
          if (state is SaveToLibraryState) {
            final snackBar = SnackBar(
              content: Text('Text saved to library!'),
              action: SnackBarAction(
                label: 'GO TO LIBRARY',
                onPressed: () {
                  var tabController = DefaultTabController.of(context);
                  if (tabController != null) tabController.animateTo(1);
                },
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        }, builder: (context, state) {
          return Stack(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: RichText(
                  text: TextSpan(
                    text: controller.paragraphs.join('\n\n'),
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, 0.9),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: (state is! SaveToLibraryState) ? Theme.of(context).primaryColor : Colors.grey,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text("SAVE TEXT TO LIBRARY"),
                  ),
                  onPressed: (state is! SaveToLibraryState)
                      ? () {
                          context.read<CaptureCubit>().saveToLibrary(controller.paragraphs);
                          context.read<LibraryCubit>().saveItem(controller.paragraphs.join('\n\n'));
                        }
                      : null,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
