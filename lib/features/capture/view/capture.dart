import 'dart:math';

import 'package:envision_ocr_assignment/core/widget_view.dart';
import 'package:envision_ocr_assignment/features/capture/domain/controller/capture_controller.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_cubit.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CaptureView extends StatefulWidgetView<Capture, CaptureController> {
  const CaptureView(CaptureController controller) : super(controller);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CaptureCubit, CapturesState>(builder: (context, state) {
        return Stack(
          children: [
            if (state is FileReadyState)
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
                child: Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(16.0),
                    child: Image.file(
                      state.image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            if (state is InitialState || state is OpenCameraState)
              Container(
                child: Center(
                  child: Icon(
                    Icons.photo,
                    size: 120,
                    color: Colors.grey.withOpacity(0.6),
                  ),
                ),
              ),
            if (state is LoadingState)
              Align(
                alignment: Alignment.center,
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Text(
                      "OCR in progress...",
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                ),
              ),
            if (state is ErrorState)
              Align(
                alignment: Alignment.center,
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Text(
                      state.message,
                      style: TextStyle(fontSize: 22, color: Colors.red.withOpacity(0.5)),
                    ),
                  ),
                ),
              ),
            if (state is LoadedState && state.eitherParagraphs.isLeft())
              Align(
                alignment: Alignment.center,
                child: Container(
                  color: Colors.red.withOpacity(0.7),
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Text(
                      state.eitherParagraphs.fold((l) => l.message.substring(0, min(l.message.length, 20)), (r) => ''),
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            if (state is InitialState || state is LoadedState)
              Align(
                alignment: Alignment(0, 0.85),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 40),
                    child: Text("CAPTURE", style: TextStyle(fontSize: 28),),
                  ),
                  onPressed: controller.openCamera,
                ),
              )
          ],
        );
      }),
    );
  }
}