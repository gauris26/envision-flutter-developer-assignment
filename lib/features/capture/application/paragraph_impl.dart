import 'package:envision_ocr_assignment/features/capture/domain/I_paragraph.dart';

class ParagraphImpl implements IParagraph {
  ParagraphImpl({required this.content, this.language = ''});

  @override
  String content;

  @override
  String language;

  @override
  String toString() {
    return 'ParagraphImpl{content: $content, language: $language}';
  }
}