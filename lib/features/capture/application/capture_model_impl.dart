import 'package:envision_ocr_assignment/features/capture/domain/I_paragraph.dart';
import 'package:envision_ocr_assignment/features/capture/domain/i_capture_model.dart';

class CaptureModelImpl implements ICaptureModel {
  final List<IParagraph> paragraphs;
  CaptureModelImpl({required this.paragraphs});
  CaptureModelImpl.empty() : paragraphs = List<IParagraph>.empty();
}