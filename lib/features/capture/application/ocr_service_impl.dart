import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'package:envision_ocr_assignment/core/constants.dart';
import 'package:envision_ocr_assignment/features/capture/domain/i_ocr_service.dart';
import 'package:envision_ocr_assignment/features/capture/domain/ocr_error.dart';
import 'package:envision_ocr_assignment/features/capture/infrastructure/dao/ocr_response_dao.dart';
import 'package:path/path.dart';
import 'dart:convert';

@Singleton(as: IOcrService)
class OcrServiceImpl implements IOcrService {
  @override
  Future<EitherCapture> process(String imagePath) async {
    var request = http.MultipartRequest(Method.post, Uri.parse(endpoint));

    var multipartFile = await http.MultipartFile.fromPath('photo', imagePath, filename: basename(imagePath));

    request.files.add(multipartFile);

    try {
      final response = await request.send();

      if (response.statusCode == 200) {
        final respStr = await response.stream.bytesToString();
        final jsonResult = jsonDecode(respStr);

        if (jsonResult['message'] != null && jsonResult['message'] == 'No text found') {
          return LeftCapture(OcrError('Text not found'));
        }

        var result = OcrResponseDao.fromJson(jsonDecode(respStr));
        return RightCapture(result.model);
      } else {
        return LeftCapture(OcrError('Unknown error please try again'));
      }
    } on SocketException {
      return LeftCapture(OcrError('No Internet connection'));
    } on FormatException {
      return LeftCapture(OcrError('Bad response format, parsing error'));
    } on HttpException {
      return LeftCapture(OcrError('Bad response format'));
    } on Exception {
      return LeftCapture(OcrError('Unknown error please try again'));
    }
  }
}
