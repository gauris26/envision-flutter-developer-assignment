import 'package:envision_ocr_assignment/features/capture/domain/extensions.dart';
export 'package:envision_ocr_assignment/features/capture/domain/extensions.dart' show EitherCapture, LeftCapture, RightCapture;

abstract class IOcrService {
  Future<EitherCapture> process(String imagePath);
}