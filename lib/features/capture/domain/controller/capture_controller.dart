import 'package:camera_camera/camera_camera.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_cubit.dart';
import 'package:envision_ocr_assignment/features/capture/view/capture.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';

class Capture extends StatefulWidget {
  Capture({Key? key}) : super(key: key);

  @override
  CaptureController createState() => CaptureController();
}

class CaptureController extends State<Capture> {
  File? selectedImage;
  bool isProgress = false;
  @override
  Widget build(BuildContext context) => CaptureView(this);

  void openCamera() async {
    context.read<CaptureCubit>().openCamera();

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => CameraCamera(
          onFile: (file) async {
            /*setState(() {
              isProgress = true;
              selectedImage = file;
            });*/
            //var result = await getIt<IOcrService>().process(file.path);
            //result.fold((l) => print(l.message), (r) => print(r.paragraphs.join()));
            context.read<CaptureCubit>().processImage(file);
            Navigator.pop(context);

          },
        ),
      ),
    );
  }
}