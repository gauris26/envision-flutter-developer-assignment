import 'package:camera_camera/camera_camera.dart';
import 'package:envision_ocr_assignment/core/widget_view.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CameraWidget extends StatelessWidget {
  CameraWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => CameraControllerView(this);
}

class CameraControllerView extends StatelessWidgetView<CameraWidget> {
  const CameraControllerView(CameraWidget controller) : super(controller);

  @override
  Widget build(BuildContext context) {
    return CameraCamera(
      onFile: (file) async {
        context.read<CaptureCubit>().processImage(file);
        //Navigator.pop(context);
      },
      /*
          setState(() {
            selectedImage = file;
          });
          result.fold((l) => print(l.message), (r) => print(r.paragraphs.join()));
          Navigator.pop(context);
        },*/
    );
  }
}