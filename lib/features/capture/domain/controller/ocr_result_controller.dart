import 'package:envision_ocr_assignment/features/capture/view/ocr_result.dart';
import 'package:flutter/material.dart';

class OcrResult extends StatelessWidget {
  OcrResult({Key? key, required this.paragraphs}) : super(key: key);
  final List<String> paragraphs;

  @override
  Widget build(BuildContext context) => OcrResultView(this);
}