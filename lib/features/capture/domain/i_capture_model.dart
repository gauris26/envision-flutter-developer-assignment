import 'I_paragraph.dart';

abstract class  ICaptureModel {
  List<IParagraph> get paragraphs;
}