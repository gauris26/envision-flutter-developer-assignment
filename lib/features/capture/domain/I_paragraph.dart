abstract class IParagraph {
  String get content;
  String get language;
}