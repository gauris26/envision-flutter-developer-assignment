import 'package:dartz/dartz.dart';
import 'package:envision_ocr_assignment/core/failure.dart';
import 'package:envision_ocr_assignment/features/capture/application/capture_model_impl.dart';
import 'i_capture_model.dart';

typedef EitherCapture = Either<Failure, ICaptureModel>;

typedef LeftCapture = Left<Failure, ICaptureModel>;

typedef RightCapture = Right<Failure, ICaptureModel>;

extension EitherGetOrElse on Either<Failure, ICaptureModel> {
  List<String> get paragraphList =>
      getOrElse(() => CaptureModelImpl.empty()).paragraphs.map<String>((e) => e.content).toList();
}
