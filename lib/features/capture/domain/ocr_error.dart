import 'package:envision_ocr_assignment/core/failure.dart';

class OcrError implements Failure {
  final String message;
  OcrError(this.message);

  @override
  String toString() {
    return 'OcrError{message: $message}';
  }
}