import 'package:intl/intl.dart';

///OCR Processing endpoint
const endpoint = "https://letsenvision.app/api/test/readDocument";

abstract class Method {
  static const post = 'POST';
  static const get = 'GET';
  static const pu = 'PUT';
  static const delete = 'DELETE';
}

const stringItemsKey = "LIBRARY_ITEMS";

final keyFormat = DateFormat('yyyy-MM-dd hh:mm:ss');
