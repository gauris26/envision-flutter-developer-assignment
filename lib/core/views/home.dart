import 'package:envision_ocr_assignment/features/capture.dart';
import 'package:envision_ocr_assignment/features/capture/domain/controller/ocr_result_controller.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_cubit.dart';
import 'package:envision_ocr_assignment/features/capture/state/capture_state.dart';
import 'package:envision_ocr_assignment/features/library/domain/controller/library_controller.dart';
import 'package:envision_ocr_assignment/features/library/state/library_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../injection.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int selectedIndex = 1;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CaptureCubit>(
          create: (_) => getIt<CaptureCubit>(),
        ),
        BlocProvider<LibraryCubit>(
          create: (_) => getIt<LibraryCubit>()..retrieve(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFF6200EE),
          accentColor: Color(0xFFFFFFFF),
        ),
        home: DefaultTabController(
          length: 2,
          initialIndex: selectedIndex,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(50.0),
              child: AppBar(
                bottom: TabBar(
                  tabs: [
                    Tab(
                      text: "CAPTURE",
                    ),
                    Tab(
                      text: "LIBRARY",
                    ),
                  ],
                ),
              ),
            ),
            body: BlocConsumer<CaptureCubit, CapturesState>(
              listener: (context, state) {
                print(state);
              },
              builder: (context, state) {
                return TabBarView(
                  children: [
                    if (state is ParagraphState && state.isValid)
                      OcrResult(paragraphs: state.paragraphs)
                    else
                      Capture(),
                    Library(),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
