// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'core/shared_preferences_helper.dart' as _i6;
import 'features/capture/application/ocr_service_impl.dart' as _i7;
import 'features/capture/domain/i_ocr_service.dart' as _i4;
import 'features/capture/state/capture_cubit.dart' as _i3;
import 'features/library/state/library_cubit.dart'
    as _i5; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.CaptureCubit>(
      () => _i3.CaptureCubit(ocrService: get<_i4.IOcrService>()));
  gh.factory<_i5.LibraryCubit>(
      () => _i5.LibraryCubit(get<_i6.SharedPreferencesHelper>()));
  gh.singleton<_i4.IOcrService>(_i7.OcrServiceImpl());
  gh.singleton<_i6.SharedPreferencesHelper>(_i6.SharedPreferencesHelper());
  return get;
}
