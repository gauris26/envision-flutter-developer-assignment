
import 'package:flutter/material.dart';

import 'core/views/home.dart';
import 'injection.dart';

void main() {
  configureDependencies(Env.dev);
  runApp(Home());
}